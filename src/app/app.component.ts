import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { EmailProvider } from "../providers/email/email";

import { HomePage } from '../pages/home/home';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  // @ViewChild('tienda') tienda: ElementRef

  rootPage: any = HomePage;
  tiendas: Array<{valor: string}>;
  pages: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public menuCtrl: MenuController,
    private _email: EmailProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Blog', component: 'BlogPage' },
      { title: 'Consejos de viajes', component: 'ConsejosPage' },
      { title: 'Guías', component: HomePage }
    ];
    this.tiendas =[
      {valor: 'Gatgets'},
      {valor: 'Maletas'},
      {valor: 'Mochilas'}
    ]

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

  }

  openPages(page) {
    this.nav.setRoot(page.component);
  }
  openTienda(value){
    switch (value) {
      case 'Gatgets':
        this.nav.setRoot('GatgetsPage')
        this.menuCtrl.close();
        break;
      case 'Maletas':
        this.nav.setRoot('MaletasPage')
        this.menuCtrl.close();
        break;
      case 'Mochilas':
        this.nav.setRoot('MochilasPage')
        this.menuCtrl.close();
        break;

      default:
      this.menuCtrl.close();
        break;
    }
  }
  sendeEmail(){
    this._email.configEmail();
  }
}
