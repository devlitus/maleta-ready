import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { ModalDetallePageModule } from "../pages/modal-detalle/modal-detalle.module";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SocialSharing } from '@ionic-native/social-sharing';
import { EmailComposer } from '@ionic-native/email-composer';
import { WordpressProvider } from '../providers/wordpress/wordpress';
import { BlogProvider } from '../providers/wordpress/blog';
import { ConsejosPrivider } from "../providers/wordpress/consejos";
import { EmailProvider } from '../providers/email/email';
import { MaletasProvider } from '../providers/wordpress/maleta';
import { MochilaProvider } from '../providers/wordpress/mochila';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ModalDetallePageModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      backButtonIcon: 'ios-arrow-back-outline',
      iconMode: 'md',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      menuType: 'reveal',
      pageTransition: 'md-transition'
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    WordpressProvider,
    BlogProvider,
    ConsejosPrivider,
    EmailComposer,
    SocialSharing,
    EmailProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}
