import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Content } from 'ionic-angular';
import { WordpressProvider } from "../../providers/wordpress/wordpress";

@IonicPage()
@Component({
  selector: 'page-rutas',
  templateUrl: 'rutas.html',
})
export class RutasPage {
  @ViewChild(Content) Content: Content;
  public rutas: Array<any> = [];
  public titulo: string = this.navParams.get('titulo');
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _serviceWp: WordpressProvider,
    public toastCtrl: ToastController) { }

  ionViewDidLoad() {
    this.getRutas();
  }
  scrollTop() {
    this.Content.scrollTo(0, 0);
  }
  getRutas() {
    let continente = this.navParams.get('guia');
    this._serviceWp.guiaRutas(continente)
      .then(res => {
        Object.assign(this.rutas, res);
        this.getMedia(res);
      });
  }
  getMedia(rutas) {
    for (const iter of rutas) {
      if (iter.featured_media) {
        this._serviceWp.guiaRutasImagenes(iter.featured_media)
          .then((res) => {
            let media = res;
            for (const key in rutas) {
              if (rutas.hasOwnProperty(key)) {
                if (rutas[key].featured_media === media['idMedia']) {
                  Object.assign(this.rutas[key], media);
                }
              }
            }
          })
          .catch(() => this.cearToast('Disculpen las Molestias'));
      }
    }
    console.log(this.rutas);
  }
  openDetalle(tags, titulo){
    this.navCtrl.push('DetallesPage', {tags, titulo})
  }
  cearToast(mensaje) {
    const toas = this.toastCtrl.create({
      message: mensaje,
      duration: 3000
    });
    toas.present();
  }

}
