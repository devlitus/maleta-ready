import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GatgetsPage } from './gatgets';

@NgModule({
  declarations: [
    GatgetsPage,
  ],
  imports: [
    IonicPageModule.forChild(GatgetsPage),
  ],
})
export class GatgetsPageModule {}
