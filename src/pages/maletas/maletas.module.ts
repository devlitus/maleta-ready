import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaletasPage } from './maletas';
import { MaletasProvider } from "../../providers/wordpress/maletas";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    MaletasPage,
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(MaletasPage),
  ],
  providers: [MaletasProvider]
})
export class MaletasPageModule {}
