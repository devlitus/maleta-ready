import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, FabContainer, Content } from 'ionic-angular';
import { MaletasProvider } from "../../providers/wordpress/maletas";
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-maletas',
  templateUrl: 'maletas.html',
})
export class MaletasPage {
  @ViewChild(Content) Content: Content;
  maletas: Array<any> = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    private socialSharing: SocialSharing,
    private _serviceMaletas: MaletasProvider) {
  }
  scrollTop() {
    this.Content.scrollTo(0, 0);
  }
  ionViewDidLoad() {
    this.getMaletas();
  }

  getMaletas(){
    this._serviceMaletas.maletas()
    .then(res => {
      Object.assign(this.maletas, res);
      this.mediaMaletas(res);
    })
  }
  mediaMaletas(maleta) {
    for (const iter of maleta) {
      if (iter.featured_media) {
        this._serviceMaletas.maletasImagenes(iter.featured_media)
          .then((res) => {
            let media = res;
            for (const key in maleta) {
              if (maleta.hasOwnProperty(key)) {
                if (maleta[key].featured_media === media['idMedia']) {
                  Object.assign(this.maletas[key], media);
                }
              }
            }
          })
          .catch(() => this.cearToast('Disculpen las Molestias'));
      }
    }
    console.log(this.maletas);
  }
  openSocial(network: string, post: any, imagen: any, fab: FabContainer) {
    switch (network) {
      case "facebook":
        this.socialSharing
          .shareViaFacebook(post.title.rendered, imagen, post.link)
          .then(() => {
            // Success!
            console.log("Share in " + network);
          })
          .catch(() => {
            // Error!
            this.cearToast("No dispones de esta aplicación");
            console.error("No se puede compartir");
          });

        break;
      case "twitter":
        this.socialSharing
        .shareViaTwitter(post.title.rendered, imagen, post.link)
        .then(() => {
          console.log("Share in " + network);
            // Success!
          })
          .catch(() => {
            // Error!
            this.cearToast("No dispones de esta aplicación");
            console.error("No se puede compartir");
          });
        break;
      case "whatsapp":
        this.socialSharing
          .shareViaWhatsApp(post.title.rendered, imagen, post.link)
          .then(() => {
            console.log("Share in " + network);
            // Success!
          })
          .catch(() => {
            // Error!
            this.cearToast("No dispones de esta aplicación");
            console.error("No se puede compartir");
          });
        break;
      case "googleplus":
        this.socialSharing
        .shareVia('com.google.android.apps.plus', post.title.rendered, imagen, post.link)
        .then(() => {
          this.cearToast("correcto");
        })
        .catch(() => {
          this.cearToast("No dispones de esta aplicación");
        });
      break;
      default:
        break;
    }
    fab.close();
  }
  cearToast(mensaje) {
    const toas = this.toastCtrl.create({
      message: mensaje,
      duration: 3500
    });
    toas.present();
  }


}
