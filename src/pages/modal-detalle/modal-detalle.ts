import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { BlogProvider } from "../../providers/wordpress/blog"

@IonicPage()
@Component({
  selector: 'page-modal-detalle',
  templateUrl: 'modal-detalle.html',
})
export class ModalDetallePage {
  postDetalle= {};
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _serviceBlog: BlogProvider,
    public viewCrtl: ViewController
    ) { }

  ionViewDidLoad() {
    this.detalleBlogPost();
  }
  detalleBlogPost(){
    let id = this.navParams.get('id');
    this._serviceBlog.blogPost(id)
    .then(res => {
      Object.assign(this.postDetalle,{titulo:res['title']['rendered'], descripcion:res['content']['rendered']});
      this.image(res);
    })
  }
  image(post){
    this._serviceBlog.guiaBlogImagenes(post.featured_media)
    .then(res => {
      Object.assign(this.postDetalle, res)
    })
    console.log(this.postDetalle);
  }
  cerrar(){
    this.viewCrtl.dismiss();
  }

}
