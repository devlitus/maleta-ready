import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalDetallePage } from './modal-detalle';
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    ModalDetallePage
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(ModalDetallePage),
  ],
})
export class ModalDetallePageModule {}
