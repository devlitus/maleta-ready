import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController, Content } from 'ionic-angular';
import { BlogProvider } from "../../providers/wordpress/blog";
import { ModalDetallePage } from "../modal-detalle/modal-detalle";

@IonicPage()
@Component({
  selector: 'page-blog',
  templateUrl: 'blog.html',
})
export class BlogPage {
  @ViewChild(Content) Content: Content;
  pageBlog = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _serviceBlog: BlogProvider,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController) { }

  ionViewDidLoad() {
    this.blog();
  }
  scrollTop() {
    this.Content.scrollTo(0, 0);
  }
  blog(){
    this._serviceBlog.blogCategoria(72)
    .then(res => {
      Object.assign(this.pageBlog, res);
      // console.log(res);
      this.getMedia(res)
    })
    .catch(() => this.cearToast('Disculpen las Molestias'));
  }
  getMedia(rutas) {
    for (const iter of rutas) {
      if (iter.featured_media) {
        this._serviceBlog.guiaBlogImagenes(iter.featured_media)
          .then((res) => {
            let media = res;
            for (const key in rutas) {
              if (rutas.hasOwnProperty(key)) {
                if (rutas[key].featured_media === media['idMedia']) {

                  Object.assign(this.pageBlog[key], media);
                }
              }
            }
          })
          .catch(() => this.cearToast('Disculpen las Molestias'));
      }
    }
    console.log(this.pageBlog);
  }
  cearToast(mensaje) {
    const toas = this.toastCtrl.create({
      message: mensaje,
      duration: 3500
    });
    toas.present();
  }
  modalDetalle(id){
    let modal = this.modalCtrl.create(ModalDetallePage, {id});
    modal.present()
  }
}
