import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { EmailProvider } from "../../providers/email/email";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('guia') guia: ElementRef;

  constructor(public navCtrl: NavController, private _email: EmailProvider) {

  }
  openPages(guia, titulo){
    this.navCtrl.push('RutasPage', {guia, titulo})
  }
  sendeEmail(){
    this._email.configEmail();
  }

}
