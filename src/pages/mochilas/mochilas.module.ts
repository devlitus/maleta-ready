import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MochilasPage } from './mochilas';
import { MochilasProvider } from "../../providers/wordpress/mochilas";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    MochilasPage,
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(MochilasPage),
  ],
  providers: [
    MochilasProvider
  ]
})
export class MochilasPageModule {}
