import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, FabContainer, Content, ToastController } from 'ionic-angular';
import { MochilasProvider } from "../../providers/wordpress/mochilas";
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-mochilas',
  templateUrl: 'mochilas.html',
})
export class MochilasPage {
  @ViewChild(Content) Content: Content;
  public mochilas: Array<any> = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private socialSharing: SocialSharing,
    public toastCtrl: ToastController,
    private _serverMochila: MochilasProvider) {
  }
  scrollTop() {
    this.Content.scrollTo(0, 0);
  }
  ionViewDidLoad() {
    this.getMochilas();
  }
  getMochilas(){
    this._serverMochila.mochilas()
    .then(res => {
      Object.assign(this.mochilas, res);
      this.getImagenes(res);
    })
  }
  getImagenes(mochilasImagen){
    for (const iter of mochilasImagen) {
    if (iter.featured_media) {
     this._serverMochila.mochilasImagenes(iter.featured_media)
      .then((res) => {
        let media = res;
        for (const key in mochilasImagen) {
          if (mochilasImagen.hasOwnProperty(key)) {
            if (mochilasImagen[key].featured_media === media['idMedia']) {
                  Object.assign(this.mochilas[key], media);
                }
              }
            }
          })
          // .catch(error => this.cearToast('Disculpen las Molestias'));
      }
    }
    console.log(this.mochilas);
  }
  openSocial(network: string, post: any, imagen: any, fab: FabContainer) {
    switch (network) {
      case "facebook":
        this.socialSharing
          .shareViaFacebook(post.title.rendered, imagen, post.link)
          .then(() => {
            // Success!
            console.log("Share in " + network);
          })
          .catch(() => {
            // Error!
            this.cearToast("No dispones de esta aplicación");
            console.error("No se puede compartir");
          });

        break;
      case "twitter":
        this.socialSharing
        .shareViaTwitter(post.title.rendered, imagen, post.link)
        .then(() => {
          console.log("Share in " + network);
            // Success!
          })
          .catch(() => {
            // Error!
            this.cearToast("No dispones de esta aplicación");
            console.error("No se puede compartir");
          });
        break;
      case "whatsapp":
        this.socialSharing
          .shareViaWhatsApp(post.title.rendered, imagen, post.link)
          .then(() => {
            console.log("Share in " + network);
            // Success!
          })
          .catch(() => {
            // Error!
            this.cearToast("No dispones de esta aplicación");
            console.error("No se puede compartir");
          });
        break;
      case "googleplus":
        this.socialSharing
        .shareVia('com.google.android.apps.plus', post.title.rendered, imagen, post.link)
        .then(() => {
          this.cearToast("correcto");
        })
        .catch(() => {
          this.cearToast("No dispones de esta aplicación");
        });
      break;
      default:
        break;
    }
    fab.close();
  }
  cearToast(mensaje) {
    const toas = this.toastCtrl.create({
      message: mensaje,
      duration: 3500
    });
    toas.present();
  }

}
