import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, FabContainer, Content, ToastController } from 'ionic-angular';
import { WordpressProvider } from "../../providers/wordpress/wordpress";
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-detalles',
  templateUrl: 'detalles.html',
})
export class DetallesPage {
  @ViewChild(Content) Content: Content;
  public detalleRutas: Array<any> = [];
  public titulo: string = this.navParams.get('titulo');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _serviceWp: WordpressProvider,
    private socialSharing: SocialSharing,
    public toastCtrl: ToastController) {
  }
  scrollTop() {
    this.Content.scrollTo(0, 0);
  }
  ionViewDidLoad() {
    this.getDetalleRuta()
  }
  getDetalleRuta(){
    let pais = this.navParams.get('tags')
    this._serviceWp.rutasDetalle(pais)
    .then(res =>{
      Object.assign(this.detalleRutas, res);
      this.getDetalleImagenes(res);
    })
  }
  getDetalleImagenes(rutas){
    for (const iter of rutas) {
      if (iter.featured_media) {
        this._serviceWp.guiaRutasImagenes(iter.featured_media)
          .then((res) => {
            let media = res;
            for (const key in rutas) {
              if (rutas.hasOwnProperty(key)) {
                if (rutas[key].featured_media === media['idMedia']) {
                  Object.assign(this.detalleRutas[key], media);
                }
              }
            }
          })
          .catch(() => this.cearToast('Disculpen las Molestias'));
      }
    }
    console.log(this.detalleRutas);
  }
  openSocial(network: string, post: any, imagen: any, fab: FabContainer) {
    switch (network) {
      case "facebook":
        this.socialSharing
          .shareViaFacebook(post.title.rendered, imagen, post.link)
          .then(() => {
            // Success!
            console.log("Share in " + network);
          })
          .catch(() => {
            // Error!
            this.cearToast("No dispones de esta aplicación");
            console.error("No se puede compartir");
          });

        break;
      case "twitter":
        this.socialSharing
        .shareViaTwitter(post.title.rendered, imagen, post.link)
        .then(() => {
          console.log("Share in " + network);
            // Success!
          })
          .catch(() => {
            // Error!
            this.cearToast("No dispones de esta aplicación");
            console.error("No se puede compartir");
          });
        break;
      case "whatsapp":
        this.socialSharing
          .shareViaWhatsApp(post.title.rendered, imagen, post.link)
          .then(() => {
            console.log("Share in " + network);
            // Success!
          })
          .catch(() => {
            // Error!
            this.cearToast("No dispones de esta aplicación");
            console.error("No se puede compartir");
          });
        break;
      case "googleplus":
        this.socialSharing
        .shareVia('com.google.android.apps.plus', post.title.rendered, imagen, post.link)
        .then(() => {
          this.cearToast("correcto");
        })
        .catch(() => {
          this.cearToast("No dispones de esta aplicación");
        });
      break;
      default:
        break;
    }
    fab.close();
  }
  cearToast(mensaje) {
    const toast = this.toastCtrl.create({
      message: mensaje,
      duration: 3500
    });
    toast.present();
  }

}
