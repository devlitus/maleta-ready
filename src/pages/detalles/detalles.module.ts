import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetallesPage } from './detalles';
import { PipesModule } from "../../pipes/pipes.module";


@NgModule({
  declarations: [
    DetallesPage,
    // SanitizeHtmlPipe
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(DetallesPage),
  ],
})
export class DetallesPageModule {}
