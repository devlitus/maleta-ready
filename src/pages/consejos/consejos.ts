import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Content, ModalController } from 'ionic-angular';
import { ConsejosPrivider } from "../../providers/wordpress/consejos";
import { ModalDetallePage } from "../modal-detalle/modal-detalle";


@IonicPage()
@Component({
  selector: 'page-consejos',
  templateUrl: 'consejos.html',
})
export class ConsejosPage {
  @ViewChild(Content) Content: Content;
  consejos: Array<any> = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    private _serviceConsejos: ConsejosPrivider) {
  }
  scrollTop() {
    this.Content.scrollTo(0, 0);
  }

  ionViewDidLoad() {
    this.getConsejos();
  }

  getConsejos() {
    this._serviceConsejos.consejos()
      .then(res => {
        Object.assign(this.consejos, res);
        this.consejosImagenes(res);
      })
  }
  consejosImagenes(imagenes) {
    for (const iter of imagenes) {
      if (iter.featured_media) {
        this._serviceConsejos.consejosImagenes(iter.featured_media)
          .then((res) => {
            let media = res;
            for (const key in imagenes) {
              if (imagenes.hasOwnProperty(key)) {
                if (imagenes[key].featured_media === media['idMedia']) {

                  Object.assign(this.consejos[key], media);
                }
              }
            }
          })
          .catch(() => this.cearToast('Disculpen las Molestias'));
      }
    }
    console.log(this.consejos);
  }
  modalDetalle(id) {
    let modal = this.modalCtrl.create(ModalDetallePage, { id });
    modal.present()
  }
  cearToast(mensaje) {
    const toas = this.toastCtrl.create({
      message: mensaje,
      duration: 3500
    });
    toas.present();
  }


}
