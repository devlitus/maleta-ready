import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class WordpressProvider {
  private url = "https://maletaready.com/wp-json/wp/v2/";
  public categorias = "categories";
  public media = "media/";
  public post = "posts";
  public tags = "tags";
  public por_pagina = "per_page=100";
  public orderby = "orderby=slug";

  constructor(public http: HttpClient) { }
  guiaRutas(continente) {
    const promise = new Promise((resolve, reject) => {
      let rutas = {};
      this.http.get(`${this.url}${this.post}?${this.categorias}=${continente}&${
        this.por_pagina}&${this.orderby}`)
        .subscribe(res => {
          let ruta = Object.assign(res);
          rutas = ruta.filter(e => {
            for (const iter of e.categories) {
              if (iter === 114) {
                return e;
              }
            }
          });
          resolve(rutas);
        });
    });
    return promise;
  }
  rutasDetalle(tags){
    const promise = new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.post}?${this.tags}=${tags}&${
        this.por_pagina}`)
        .subscribe(res => {
          // console.log(res);
          /* let detalles = Object.assign(res);
          detalle = detalles.filter(e =>{
            for (const iter of e.categories) {
              if (iter === 98) {
                e
              }
            }

          }) */
          resolve(res);
        });
    });
    return promise;
  }
  guiaRutasImagenes(imagenes) {
    const promise = new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.media}${imagenes}`)
        .subscribe(res => {
          let post = { idMedia: res['id'], source: res['source_url'] }
          resolve(post);
        })
    })
    return promise
  }

}
