import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class MochilasProvider {
  private url = "https://maletaready.com/wp-json/wp/v2/";
  public categorias = "categories";
  public media = "media/";
  public post = "posts";
  public pages = "pages";
  public por_pagina = "per_page=100";
  public orderby = "orderby=slug";

  constructor(private _http: HttpClient){}

  mochilas(){
    const promise = new Promise((resolve, reject) => {
      this._http.get(`${this.url}${this.post}?tags=216&categories=116`)
      .subscribe(res => {
        resolve(res);
      })
    })
    return promise;
  }
  mochilasImagenes(imagenes) {
    const promise = new Promise((resolve, reject) => {
      this._http.get(`${this.url}${this.media}${imagenes}`)
      .subscribe(res => {

          let post = { idMedia: res['id'], source: res['source_url'] }
          resolve(post);
        })
    })
    return promise
  }


}
