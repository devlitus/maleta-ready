import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class MaletasProvider {
  private url = "https://maletaready.com/wp-json/wp/v2/";
  public categorias = "categories";
  public media = "media/";
  public post = "posts";
  public pages = "pages";
  public por_pagina = "per_page=100";
  public orderby = "orderby=slug";

  constructor(public http: HttpClient) { }

  maletas(){
    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.post}?${this.categorias}=84&tags=219`)
      .subscribe(res => {
        resolve(res);
      })
    });
  }
  maletasImagenes(imagenes) {
    const promise = new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.media}${imagenes}`)
        .subscribe(res => {
          let post = { idMedia: res['id'], source: res['source_url'] }
          resolve(post);
        })
    })
    return promise
  }


}
